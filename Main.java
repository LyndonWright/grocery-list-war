
/**********************************************
 * Program: Grocery List War.Java
 * Files: - Main.java
 * - Produce.java
 * - Apple.java
 * - Carrot.java
 * - Walnut.java
 * Author: Lyndon Wright
 * Course: CIDS 235
 * Assignment: Assignment 2 - Grocery List War
 * Date Created: 11/06/2023
 * Date Modified: 11/19/2023
 * Description: This program uses the information
 * from the Produce.java in order to read how
 * they attack each other.
 **********************************************/

import java.util.ArrayList;

class Main {
  public static void main(String[] args) {

    ArrayList<Produce> produceList = new ArrayList<>();

    Produce apple = new Apple();
    Produce carrot = new Carrot();
    Produce walnut = new Walnut();

    // Add produce items to produceList
    produceList.add(apple);
    produceList.add(carrot);
    produceList.add(walnut);

    // Choose the Attacker and the Defender from the produceList
    Produce attacker = produceList.get((int) (Math.random() * produceList.size()));
    // System.out.println("The Attacker is: " + attacker.getName());

    Produce defender = produceList.get((int) (Math.random() * produceList.size()));
    // System.out.println("The Defender is: " + defender.getName());

    if (attacker == defender) {
      attacker = produceList.get((int) (Math.random() * produceList.size()));
      defender = produceList.get((int) (Math.random() * produceList.size()));
    }
    System.out.println(attacker.toString());
    System.out.println(defender.toString());

    // Battle
    while (attacker.getHealth() >= 0 && defender.getHealth() >= 0) {
      attacker.attack(defender);
      System.out.println(attacker.getName() + " attacks " + defender.getName() + ".");
      System.out.println(defender.getName() + " has " + defender.getHealth() + " health remaining.");
      System.out.println(defender.getName() + " took damage from " + attacker.getName() + ".");
      defender.attack(attacker);
      System.out.println(defender.getName() + " attacks " + attacker.getName() + ".");

      // Defender attacks Attacker
      if (defender.getHealth() != 0) {
        defender.attack(attacker);
      }
    }

    if (attacker.getHealth() > defender.getHealth()) {
      System.out.println(attacker.getName() + " is the Winner!");
      return;
    }
    if (defender.getHealth() > attacker.getHealth()) {
      System.out.println(defender.getName() + " is the Winner!");
      return;
    }

  }
}