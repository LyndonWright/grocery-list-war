/**********************************************
 * Program: Grocery List War.Java
 * Files: - Walnut.java
 * - Main.java
 * - Produce.java
 * - Apple.java
 * - Carrot.java
 * Author: Lyndon Wright
 * Course: CIDS 235
 * Assignment: Assignment 2 - Grocery List War
 * Date Created: 11/06/2023
 * Date Modified: 11//2023
 * Description: This program gets the ability
 * and description of the Walnut in the war.
 **********************************************/

class Walnut extends Produce {

  // instances
  private String name = "Walnut";
  private int health = 5;
  private int attack = 1;
  private int slam = 4;

  // Methods
  public String getName() {
    return name;
  }

  public int getHealth() {
    return health;
  }

  public int getAttack() {
    return attack;
  }

  public int getAbility() {
    return slam;
  }

  @Override
  public int attack(Produce opponent) {
    opponent.takeDamage(attack + slam);
    return attack;
  }

} // end of class Walnut