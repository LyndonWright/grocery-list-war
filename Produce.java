
/**********************************************
Program: Grocery List War.Java
  Files: - Produce.java
           - Main.java
           - Apple.java
           - Carrot.java
           - Walnut.java
  Author: Lyndon Wright
  Course: CIDS 235
  Assignment: Assignment 2 - Grocery List War
  Date Created: 11/06/2023
  Date Modified: 11//2023
  Description: This program gets the information
 from the subclasses Apple, Carrot, and Walnut 
 in order to read how they attack each other.
**********************************************/
import java.util.ArrayList;

public class Produce {

  // Instances
  private String name;
  private int health;
  private int attack;
  private int ability;

  // Constructors
  // Default Constructor
  public Produce() {
  }

  public Produce(String name, int health, int attack, int ability) {
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.ability = ability;
  }

  // Methods
  public String getName() {
    return name;
  }

  public int getHealth() {
    return health;
  }

  public int getAttack() {
    return attack;
  }

  public int getAbility() {
    return ability;
  }

  public int attack(Produce opponent) {
    return attack;
  }

  public int takeDamage(int damage) {
    damage = getHealth() - getAttack();
    return damage;
  }

  @Override
  public String toString() {
    return "Name: " + getName() +
        "\nHealth: " + getHealth() +
        "\nAttack: " + getAttack() +
        "\nAbility: " + getAbility() +
        "\n";
  }

} // end of class Produce