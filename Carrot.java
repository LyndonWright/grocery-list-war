/**********************************************
 * Program: Grocery List War.Java
 * Files: - Carrot.java
 * - Main.java
 * - Produce.java
 * - Apple.java
 * - Walnut.java
 * Author: Lyndon Wright
 * Course: CIDS 235
 * Assignment: Assignment 2 - Grocery List War
 * Date Created: 11/06/2023
 * Date Modified: 11//2023
 * Description: This program gets the ability
 * and description of the Carrots in the war.
 **********************************************/

class Carrot extends Produce {

  // instances
  private String name = "Carrot";
  private int health = 1;
  private int attack = 3;
  private int pierce = 7;

  // Methods
  public String getName() {
    return name;
  }
public int getHealth() {
    return health;
  }

  public int getAttack() {
    return attack;
  }

  public int getAbility() {
    return pierce;
  }

  @Override
  public int attack(Produce opponent) {
    opponent.takeDamage(attack + pierce);
    return attack;
  }

} // end of class Carrot