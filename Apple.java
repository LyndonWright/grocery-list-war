/**********************************************
 * Program: Grocery List War.Java
 * Files: - Apple.java
 * - Main.java
 * - Produce.java
 * - Carrot.java
 * - Walnut.java
 * Author: Lyndon Wright
 * Course: CIDS 235
 * Assignment: Assignment 2 - Grocery List War
 * Date Created: 11/06/2023
 * Date Modified: 11//2023
 * Description: This program gets the ability
 * and description of the Apples in the war.
 **********************************************/

class Apple extends Produce {

  // instances
  private String name = "Apple";
  private int health = 4;
  private int attack = 2;
  private int charge = 3;

  // Methods
  public String getName() {
    return name;
  }

  public int getHealth() {
    return health;
  }

  public int getAttack() {
    return attack;
  }

  public int getAbility() {
    return charge;
  }

  @Override
  public int attack(Produce opponent) {
    System.out.println("Apple attacks " + opponent.getName());
    opponent.takeDamage(attack + charge);
    return attack;
  }

} // end of class Apple